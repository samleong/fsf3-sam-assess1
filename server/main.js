/**
 * Created by sswl on 17/10/16.
 */
// Include express module
var express = require("express");

// Create APP
var app = express();

// CREATE a config for PORT
const PORT = 3000;
const CLIENT_FOLDER = __dirname + "/../client";

console.log("Serving Client folder at ", CLIENT_FOLDER);

app.use(express.static(CLIENT_FOLDER));


app.use(function (err, req, res, next) {
    res.redirect("/501.html");
});

app.get(function(req, res, next){
    var reshead = {};

    console.log(req.ip);
    console.log(req.hostname);
    console.log(req.originalUrl);
    console.log(req.protocol);
    console.log(req.method);

    reshead.r1 = req.ip;
    reshead.r2 = req.hostname;
    reshead.r3 = req.originalUrl;
    reshead.r4 = req.protocol;
    reshead.r5 = req.method;

    res.json(reshead);

    next();
});


// Start server on PORT
app.listen(PORT, function () {
    console.log("Running server on http://localhost:%s", PORT);
});

